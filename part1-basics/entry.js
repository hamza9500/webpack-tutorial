/**
 * This file contains the first webpack configuration for the webpack tutorial 
 * "Built your own webpack configuration" powered by urbdio.
 *  
 * Find more on medium
 * 
 * @author Guillaume Dormoy <guillaume.dormoy@urbd.io>
 */

module.exports = {
    entry: path.resolve(__dirname, 'src') + 'file.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.js'
    }
  };